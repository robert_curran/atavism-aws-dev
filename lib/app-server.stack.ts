import cdk = require("@aws-cdk/core");
import {
    Vpc,
    Instance,
    InstanceType,
    SubnetType,
    IMachineImage,
    SecurityGroup,
    Peer,
    Port,
    CfnEIP,
    CfnInstance,
    ISecurityGroup
} from "@aws-cdk/aws-ec2";
import { IIngressPoint } from "./interfaces/IIngressPoint";

export interface AppServerStackProps extends cdk.StackProps {
    vpc: Vpc;
    sshKeyName: string;
    sshIngressPeers: IIngressPoint[];
    instanceClass: InstanceType;
    machineImage: IMachineImage;
}

export class AppServerStack extends cdk.Stack{
    public appServerSecGroup: ISecurityGroup;

    constructor(scope: cdk.Construct, id: string, props: AppServerStackProps) {
        super(scope, id, props);

        // Create security group for app server open ports for atavism server and allowing outbound traffic
        this.appServerSecGroup = new SecurityGroup(this, "AppServerSecGroup_Atavism", {
            vpc: props.vpc,
            allowAllOutbound: true
        });

        this.appServerSecGroup.addIngressRule(Peer.anyIpv4(), Port.tcpRange(5040, 5060), "Atavism");
        this.appServerSecGroup.addIngressRule(Peer.anyIpv4(), Port.udpRange(5040, 5060), "Atavism");
        this.appServerSecGroup.addIngressRule(Peer.anyIpv4(), Port.tcp(5090), "Atavism");
        this.appServerSecGroup.addIngressRule(Peer.anyIpv4(), Port.udp(5090), "Atavism");
        this.appServerSecGroup.addIngressRule(Peer.anyIpv4(), Port.tcpRange(9005, 9010), "Atavism");
        this.appServerSecGroup.addIngressRule(Peer.anyIpv4(), Port.udpRange(9005, 9010), "Atavism");

        // AppServer ec2 instance
        const ec2 = new Instance(this, "AppServer", {
            instanceType: props.instanceClass,
            machineImage: props.machineImage,
            vpc: props.vpc,
            vpcSubnets: {
                subnetType: SubnetType.PUBLIC
            },
            keyName: props.sshKeyName,
            securityGroup: this.appServerSecGroup
        });

        // Set size of root volume
        const ec2CfnInstance = ec2.node.defaultChild as CfnInstance;

        ec2CfnInstance.blockDeviceMappings = [
            {
                deviceName: "/dev/sda1",
                ebs: {
                    volumeSize: 30
                }
            }
        ];

        // Assign elastic ip address to app server
        new CfnEIP(this, "AppServiceEIP", {
            instanceId: ec2.instanceId
        });

        new cdk.CfnOutput(this, "AppServerPublicDns", {
            value: ec2.instancePublicDnsName
        });

        // Open ports for ssh
        if (props.sshIngressPeers.length) {
            const sshSecurityGroup = new SecurityGroup(this, "AppServerSecGroup_SSH", {
                vpc: props.vpc,
                allowAllOutbound: false
            });

            for (const peer of props.sshIngressPeers) {
                sshSecurityGroup.addIngressRule(Peer.ipv4(peer.cidr), Port.tcp(22), peer.name);
            }

            ec2.addSecurityGroup(sshSecurityGroup);
        }
    }
}
