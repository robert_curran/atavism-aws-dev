import cdk = require("@aws-cdk/core");
import { Vpc, SubnetType } from "@aws-cdk/aws-ec2";

export class VpcStack extends cdk.Stack{
    public vpc: Vpc;

    constructor(scope: cdk.Construct, id: string, props: cdk.StackProps) {
        super(scope, id, props);

        // Simple VPC with 1 public subnet per AZ
        this.vpc = new Vpc(this, "VPC", {
            natGateways: 0,
            subnetConfiguration: [
                {
                    cidrMask: 18,
                    name: "Public",
                    subnetType: SubnetType.PUBLIC
                }
            ]
        });
    }
}
