export interface IIngressPoint {
    cidr: string;
    name: string;
}
