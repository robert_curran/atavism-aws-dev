import cdk = require("@aws-cdk/core");
import { Vpc, InstanceType, SubnetType, SecurityGroup, Peer, Port, ISecurityGroup } from "@aws-cdk/aws-ec2";
import { DatabaseInstance, DatabaseInstanceEngine } from "@aws-cdk/aws-rds";
import { IIngressPoint } from "./interfaces/IIngressPoint";

export interface DatabaseStackProps extends cdk.StackProps {
    vpc: Vpc;
    materUsername: string;
    masterPassword: cdk.SecretValue;
    dbEngine: DatabaseInstanceEngine;
    instanceClass: InstanceType;
    ingressPoints: IIngressPoint[];
    appServerSecGroup: ISecurityGroup;
}

export class DatabaseStack extends cdk.Stack{
    constructor(scope: cdk.Construct, id: string, props: DatabaseStackProps) {
        super(scope, id, props);

        // Create RDS instance
        const rds = new DatabaseInstance(this, "Database", {
            engine: props.dbEngine,
            instanceClass: props.instanceClass,
            masterUsername: props.materUsername,
            masterUserPassword: props.masterPassword,
            vpc: props.vpc,
            allocatedStorage: 30,
            vpcPlacement: {
                subnetType: SubnetType.PUBLIC
            }
        });

        // Allow the AppServer to connect to the database
        rds.connections.allowDefaultPortFrom(props.appServerSecGroup);

        // Get reference to RDS instance security group
        const dbSecurityGroup = SecurityGroup.fromSecurityGroupId(this, "DbSecurityGroup", rds.securityGroupId);

        // Allow access to defined ingress points
        if (props.ingressPoints) {

            // Add all required ingress rules granting public access to the database
            for (const point of props.ingressPoints) {
                dbSecurityGroup.addIngressRule(Peer.ipv4(point.cidr), Port.tcp(3306), point.name);
            }
        }
    }
}
