#!/usr/bin/env node
import "source-map-support/register";
import cdk = require("@aws-cdk/core");
import { VpcStack } from "../lib/vpc.stack";
import { DatabaseStack } from "../lib/database.stack";
import { DatabaseInstanceEngine } from "@aws-cdk/aws-rds";
import { InstanceType, InstanceClass, InstanceSize, GenericLinuxImage } from "@aws-cdk/aws-ec2";
import { AppServerStack } from "../lib/app-server.stack";
import { IIngressPoint } from "../lib/interfaces/IIngressPoint";

const app = new cdk.App();

const env: cdk.Environment = {
    region: "eu-west-2"
};

const centos7 = new GenericLinuxImage({
    "eu-west-2": "ami-0eab3a90fc693af19"
});

const ingressPoints: IIngressPoint[] = [
    { cidr: "TODO", name: "TODO" }
];

const vpcStack = new VpcStack(app, "AtavismDevDemoVpc", {
    env
});

const appServerStack = new AppServerStack(app, "AtavismDevDemoAppServer", {
    env,
    vpc: vpcStack.vpc,
    sshKeyName: "TODO",
    instanceClass: InstanceType.of(InstanceClass.BURSTABLE3, InstanceSize.MEDIUM),
    machineImage: centos7,
    sshIngressPeers: ingressPoints
});

new DatabaseStack(app, "AtavismDevDemoDatabase", {
    env,
    vpc: vpcStack.vpc,
    materUsername: "Admin",
    masterPassword: new cdk.SecretValue("TODO"),
    dbEngine: DatabaseInstanceEngine.MARIADB,
    instanceClass: InstanceType.of(InstanceClass.BURSTABLE3, InstanceSize.MICRO),
    ingressPoints,
    appServerSecGroup: appServerStack.appServerSecGroup
});
