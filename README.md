# Atavism AWS Dev Infrastructure

This repository contains an AWS CDK application that builds the minimum infrastructure required to run AtavismOnline.

The application contains stacks for the VPC (network), application server and database. The configuration for each stack is defined in `bin/development.ts`.

## Prerequisits

Before you start you will need an AWS account with CLI access to authenticate the CDK commands and to have created and EC2 key pair in the AWS console for SSH access to the application server that will be created.

Additionally the application server is running a CentOS 7 image from the AWS market place you need to sign in to your AWS account and subscribe to the free image at https://aws.amazon.com/marketplace/pp/Centosorg-CentOS-7-x8664-with-Updates-HVM/B00O7WM7QW

## VPC Stack

This stack sets up a basic 1 public per availability zone VPC network where all other resources will be created.

## App Server Stack

This stack creates a single EC2 instance (CentOS 7) to run the Atavism server with all required ports opened.

## Database Stack

This stack creates an RDS instance for the Atavism database.

## Configuration

Before deploying any infrastructure you should update the configurations in `bin/development.ts` to customise the instance sizes, SSH access and database password for your installation. Ensure all `TODO` are done.

## Deployment

The AWS CDK uses CloudFormation to provision the defined infrastructure. First you must build the TypeScript application. You can ten check what will be deployed and finally deploy your infrastructure.

### Setup and build

```shell
npm i -g aws-cdk
npm i
npm run build
```

### Check what will be deployed

```shell
cdk diff
```

### Deploy everything

```shell
cdk deploy
```

### Deploy single stack

```shell
cdk deploy <stack name>
```
